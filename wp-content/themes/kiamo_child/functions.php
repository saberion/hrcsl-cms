<?php
/**
 *
 * @package [Parent Theme]
 * @author  gaviasthemes <gaviasthemes@gmail.com>
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU Public License
 * 
 */


function kiamo_child_scripts() {
   wp_enqueue_style( 'kiamo-parent-style', get_template_directory_uri(). '/style.css');
    wp_enqueue_style( 'basictable-css', get_stylesheet_directory_uri(). '/css/basictable.css');
   wp_enqueue_style( 'kiamo-child-style', get_stylesheet_uri());

    wp_enqueue_script( 'basictable-js', get_stylesheet_directory_uri() . '/js/jquery.basictable.js', array( 'jquery' ), '1.0', true );
   wp_enqueue_script( 'theme_js', get_stylesheet_directory_uri() . '/js/custom.js', array( 'jquery' ), '1.0', true );
   
}

add_action( 'wp_enqueue_scripts', 'kiamo_child_scripts', 9999 );


