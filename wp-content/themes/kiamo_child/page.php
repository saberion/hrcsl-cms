<?php
/**
 * $Desc
 *
 * @version    1.0
 * @package    basetheme
 * @author     Gaviasthemes Team
 * @copyright  Copyright (C) 2016 Gaviasthemess. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 */
?>



<?php get_header(apply_filters('kiamo_get_header_layout', null )); ?>


<?php if ( has_post_thumbnail() ) { ?>

<div class="banner fullwidth" style="background-image: url('<?php echo wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' )[0]; ?>')">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <h2><?php echo get_the_title( $post->post_parent ); ?></h2>
                <h1><?php the_title(); ?></h1>
            </div>
        </div>
    </div>
</div>

<?php
} else {

}

?>






<?php
global $post;
if ( is_page() && $post->post_parent ) { ?>


<div class="fullwidth submenubox">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">

                                <?php dynamic_sidebar( 'other_sidebar' ); ?>
            </div>
        </div>
    </div>
</div>


<?php } ?>






<?php kiamo_base_layout('page'); ?>

<?php include('newsletter.php'); ?>

<?php get_footer(); ?>
