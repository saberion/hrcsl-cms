<?php
/**
 * $Desc
 *
 * @version    1.0
 * @package    basetheme
 * @author     Gaviasthemes Team
 * @copyright  Copyright (C) 2016 Gaviasthemess. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 */
?>



<?php get_header(apply_filters('kiamo_get_header_layout', null )); ?>




<div class="banner fullwidth banner-region"  >
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <h2>Regional Offices</h2>
                <h1><?php the_title(); ?> Regional Offices</h1>
            </div>
        </div>
    </div>
</div>

<div class="fullwidth submenubox">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">

                <?php dynamic_sidebar( 'other_sidebar' ); ?>
            </div>
        </div>
    </div>
</div>




<?php
global $post;
if ( is_page() && $post->post_parent ) { ?>


    <div class="fullwidth submenubox">
        <div class="container">
            <div class="row">
                <div class="col-md-12 ">

                    <?php dynamic_sidebar( 'other_sidebar' ); ?>
                </div>
            </div>
        </div>
    </div>


<?php } ?>



<div <?php post_class( 'clearfix' ); ?> id="<?php the_ID(); ?>">

    <?php do_action( 'kiamo_page_content_before' ); ?>

    <div class="container" >
        <div class="row divisiondata">

            <div class="col-md-9 col-sm-7">

                <div class="fullwidth division-info">
                <h3>Location</h3>
                <?php while ( have_posts() ) : the_post(); ?>
                        <?php the_content(); ?>
                        <?php
                    endwhile;
                    wp_reset_query();
                    ?>
                </div>


                <?php if( get_field('administrative_area') ):  ?>
                <div class="fullwidth division-info">
                    <h3>Administrative Area</h3>
                    <p> <?php   the_field('administrative_area');?></p>
                </div>
                <?php endif; ?>


                <?php if( have_rows('population') ):  ?>
                <div class="fullwidth division-info">
                    <h3>Population and Distribution</h3>

                    <table class="poptable poptable-desktop">
                        <thead>
                        <tr>
                            <th rowspan="2" valign="center">District</th>
                            <th colspan="5" class="popultion-title">Population Distribution</th>
                            <th rowspan="2" valign="middle">Total</th>
                        </tr>
                        <tr class="poptable-headsub">
                            <th>Sinhalese</th>
                            <th>Sri Lankan Moors</th>
                            <th>Sri Lankan Tamils</th>
                            <th>Indian Tamils</th>
                            <th>Others</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php  while ( have_rows('population') ) : the_row();
                        { ?>
                        <tr class="poprow">
                            <td class="firstrow"><?php the_sub_field('district');  ?></td>
                            <td class="pvalue"><?php the_sub_field('sinhalese');  ?></td>
                            <td class="pvalue"><?php the_sub_field('sri_lankan_moors');  ?></td>
                            <td class="pvalue"><?php the_sub_field('sri_lankan_tamils');  ?></td>
                            <td class="pvalue"><?php the_sub_field('indian_tamils');  ?></td>
                            <td class="pvalue"><?php the_sub_field('others');  ?></td>
                            <td class="total-pop"></td>
                        </tr>
                        <?php   }
                        endwhile;  ?>
                        </tbody>
                    </table>



                    <?php  while ( have_rows('population') ) : the_row();
                    { ?>
                    <table class="poptable poptable-mobile">
                        <thead>
                        <tr>
                            <th colspan="2"  class="popultion-title"><?php the_sub_field('district');  ?></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th  class="poptable-headsub">Population </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="firstrow">Sinhalese</td>
                            <td class="pvalue"><?php the_sub_field('sinhalese');  ?></td>
                        </tr>
                        <tr>
                            <td class="firstrow">Sri Lankan Moors</td>
                            <td class="pvalue"><?php the_sub_field('sri_lankan_moors');  ?></td>
                        </tr>
                        <tr>
                            <td class="firstrow">Sri Lankan Tamils</td>
                            <td class="pvalue"><?php the_sub_field('sri_lankan_tamils');  ?></td>
                        </tr>
                        <tr>
                            <td class="firstrow">Indian Tamils</td>
                            <td class="pvalue"><?php the_sub_field('indian_tamils');  ?></td>
                        </tr>
                        <tr>
                            <td class="firstrow">Others</td>
                            <td class="pvalue"><?php the_sub_field('others');  ?></td>
                        </tr>
                        <tr>
                            <td class="firstrow">Total</td>
                            <td class="pvalue-sum"></td>
                        </tr>
                        </tbody>
                    </table>
                        <?php   }
                        endwhile;  ?>

                </div>




                <?php endif; ?>




                <?php if( get_field('reports') ):  ?>
                <div class="fullwidth division-info">
                    <h3>Reports</h3>
                    <p> <?php   the_field('reports');?></p>
                </div>
                <?php endif; ?>

            </div>

            <div class="col-md-3 col-sm-5 sidebar-division">
                <div class="fullwidth">
                    <div class="contactbox">
                       <p> <?php if( get_field('address') ):  ?> <?php    the_field('address');?><br/> <?php endif; ?>
                           <?php if( get_field('phone') ):  ?>  Tel : <?php    the_field('phone');?><br/>  <?php endif; ?>
                           <?php if( get_field('fax') ):  ?>  Fax :<?php    the_field('fax');?><br/>  <?php endif; ?>
                           <?php if( get_field('email') ):  ?> Email :<a href="mailto:<?php the_field('email');?>"><?php    the_field('email');?></a>  <?php endif; ?> </p>
                    </div>


                    <h3>Other Details</h3>



                    <?php if( have_rows('police_stations') ):  ?>
                    <div class="fullwidth calbox">
                        <h4>Number of Police stations: <b class="b-total"></b> </h4>
                        <span>(
                            <?php  while ( have_rows('police_stations') ) : the_row();
                            { ?>
                                <?php the_sub_field('district'); ?>  – <b class="cal-value"><?php the_sub_field('amount'); ?></b>,
                            <?php   }
                            endwhile;  ?>
                            )</span>
                    </div>
                    <?php endif; ?>


                    <div class="fullwidth calbox">
                        <h4>Prison: <b class="b-total"></b> </h4>
                        <span>(Open Prison – <b class="cal-value"><?php the_field('open_prison'); ?></b>, Normal – <b class="cal-value"><?php the_field('normal_prisons'); ?></b>) </span>
                    </div>




                    <?php if( have_rows('other_custodial') ):  ?>
                    <div class="fullwidth">
                        <h4>Other Custodial Institutions: <b class="total-custodial"></b></h4>

                        <ul class="custodiallist"> <?php  while ( have_rows('other_custodial') ) : the_row();
                            { ?>
                            <li>( <?php the_sub_field('custodial_name'); ?>)</li>
                            <?php   }
                            endwhile;  ?>
                        </ul>
                    </div>
                    <?php endif; ?>



                    <?php if( get_field('other_custodial') ):  ?>
                    <div class="fullwidth">
                        <h4>Children Home/ Correctional Centers: <?php the_field('children_homecorrectional_centers'); ?></h4>
                    </div>
                    <?php endif; ?>

                </div>
            </div>

        </div>
    </div>



    <?php do_action( 'kiamo_page_content_after' ); ?>

</div>

<?php include('newsletter.php'); ?>

<?php get_footer(); ?>
