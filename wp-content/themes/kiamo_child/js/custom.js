(function($) {
	//show current copyright year 
	$('.copyright-year').text(new Date().getFullYear());

	//add a class to first <p> which represents the text 'subscribe to our newsletter'
	$('#mailpoet_form_1 .mailpoet_paragraph').first().addClass('form-text');






    // regional office calculations

    $('.poptable-desktop tr.poprow').each(function () {
        var sum = 0 ;
        $(this).find('.pvalue').each(function () {
            var combat = $(this).text();
            if (!isNaN(combat) && combat.length !== 0) {
                sum += parseFloat(combat);
            }
        });
        $('.total-pop', this).html(sum);
    });


    $('.poptable-mobile').each(function () {
        var msum = 0 ;
        $(this).find('.pvalue').each(function () {
            var mcombat = $(this).text();
            if (!isNaN(mcombat) && mcombat.length !== 0) {
                msum += parseFloat(mcombat);
            }
        });
        $('.pvalue-sum', this).html(msum);
    });


    $('.calbox').each(function () {
        var calsum = 0 ;
        $(this).find('.cal-value').each(function () {
            var calcombat = $(this).text();
            if (!isNaN(calcombat) && calcombat.length !== 0) {
                calsum += parseFloat(calcombat);
            }
        });
        $('.b-total', this).html(calsum);
    });

    var licount = $(".custodiallist li").length;
    $('.total-custodial').html(licount);


    $('.archive article .entry-content').each(function(){
        $(this).html('Watch the relevant video here');
    })


})( jQuery );

